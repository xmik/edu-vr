import pygame

width = 800
height = 600
disp_size = (width, height)

screen = pygame.display.set_mode(disp_size)

# Image(Surface) which will be refrenced
canvas = pygame.Surface(disp_size)

# Camera rectangles for sections of  the canvas
p1_camera = pygame.Rect(0,0,float(width)/2.0,height)
p2_camera = pygame.Rect(float(width)/2.0,0,float(width)/2.0,height)

# subsurfaces of canvas
# Note that subx needs refreshing when px_camera changes.
sub1 = canvas.subsurface(p1_camera)
sub2 = canvas.subsurface(p2_camera)

# Now drawing on any of of the subsurfaces with these normalized coordinates

# Drawing a line on each split "screen"
pygame.draw.line(sub1, (255,255,255), (0,0), (0,height), 10)
pygame.draw.line(sub2, (255,255,255), (0,0), (0,height), 10)

# draw player 1's view  to the top left corner
screen.blit(sub1, (0,0))
# player 2's view is in the top right corner
screen.blit(sub2, (float(width)/2.0, 0))

while True:
    # Update the screen
    pygame.display.update()
