# source: https://jonnoftw.github.io/2017/01/27/libfreenect-python-depth-image

import pygame
import numpy as np
import sys
from freenect import sync_get_depth as get_depth


def make_gamma():
    """
    Create a gamma table
    """
    # For the kinect v1, the raw depth values range between 0 and 2048,
    # for the kinect v2 the range is between 0 and 4500.
    # https://shiffman.net/p5/kinect/
    num_pix = 2048 # there's 2048 different possible depth values
    npf = float(num_pix)
    # https://docs.scipy.org/doc/numpy/reference/generated/numpy.empty.html
    # create an empty np array of dimensions: num_pix x 3;
    # the 3 values represent the colors
    _gamma = np.empty((num_pix, 3), dtype=np.uint16)
    for i in xrange(num_pix):
        v = i / npf
        v = pow(v, 3) * 6
        pval = int(v * 6 * 256)
        lb = pval & 0xff
        pval >>= 8
        if pval == 0:
            a = np.array([255, 255 - lb, 255 - lb], dtype=np.uint8)
        elif pval == 1:
            a = np.array([255, lb, 0], dtype=np.uint8)
        elif pval == 2:
            a = np.array([255 - lb, lb, 0], dtype=np.uint8)
        elif pval == 3:
            a = np.array([255 - lb, 255, 0], dtype=np.uint8)
        elif pval == 4:
            a = np.array([0, 255 - lb, 255], dtype=np.uint8)
        elif pval == 5:
            a = np.array([0, 0, 255 - lb], dtype=np.uint8)
        else:
            a = np.array([0, 0, 0], dtype=np.uint8)

        _gamma[i] = a
    return _gamma

def make_gamma_only_closest():
    """
    Create a gamma table
    """
    # For the kinect v1, the raw depth values range between 0 and 2048,
    # for the kinect v2 the range is between 0 and 4500.
    # https://shiffman.net/p5/kinect/
    num_pix = 2048 # there's 2048 different possible depth values
    npf = float(num_pix)
    # https://docs.scipy.org/doc/numpy/reference/generated/numpy.empty.html
    # create an empty np array of dimensions: num_pix x 3;
    # the 3 values represent the colors
    _gamma = np.empty((num_pix, 3), dtype=np.uint16)
    for i in xrange(num_pix):
        v = i / npf
        v = pow(v, 3) * 6
        pval = int(v * 6 * 256)
        lb = pval & 0xff
        pval >>= 8
        if pval == 0:
            a = np.array([255, 255 - lb, 255 - lb], dtype=np.uint8)
        else:
            a = np.array([0, 0, 0], dtype=np.uint8)

        _gamma[i] = a
    return _gamma

def get_colorful_depth(depth_frame, gamma):
    depth = np.rot90(depth_frame[0]) # get the depth readinngs from the camera
    pixels = gamma[depth] # the colour pixels are the depth readings overlayed onto the gamma table
    return pixels


class RegionOfInterest:
    def __init__(self, top, right, bottom, left):
        self.top = top
        self.right = right
        self.bottom = bottom
        self.left = left


# We split the pixels from roi into 3 parts: left, middle, right
# (height is always the same) and decide which part has the most amount
# pixels which are not 0 (which are occupied with e.g. a hand).
def which_rect_part_is_occupied_the_most(pixels, width, height, roiCoordinates):
    pixel_iter_between_left_and_middle = int(1.0/3.0 * width + roiCoordinates.left)
    pixel_iter_between_middle_and_right = int(2.0/3.0 * width + roiCoordinates.left)

    left_pixels_count = 0
    left_pixels_count_not_0 = 0
    for i in range(roiCoordinates.left, pixel_iter_between_left_and_middle):
        for j in range(roiCoordinates.top, roiCoordinates.bottom+1):
            pixel = pixels[i][j]
            left_pixels_count += 1
            if pixel[0] != 0 and pixel[1] != 0:
                left_pixels_count_not_0 += 1

    middle_pixels_count = 0
    middle_pixels_count_not_0 = 0
    for i in range(pixel_iter_between_left_and_middle+1, pixel_iter_between_middle_and_right):
        for j in range(roiCoordinates.top, roiCoordinates.bottom+1):
            pixel = pixels[i][j]
            middle_pixels_count += 1
            if pixel[0] != 0 and pixel[1] != 0:
                middle_pixels_count_not_0 += 1

    right_pixels_count = 0
    right_pixels_count_not_0 = 0
    for i in range(pixel_iter_between_middle_and_right+1, roiCoordinates.right+1):
        for j in range(roiCoordinates.top, roiCoordinates.bottom+1):
            pixel = pixels[i][j]
            right_pixels_count += 1
            if pixel[0] != 0 and pixel[1] != 0:
                right_pixels_count_not_0 += 1

    print("left pixels count (not 0/ all): " + str(left_pixels_count_not_0) + "/" + str(left_pixels_count))
    print("middle pixels count (not 0/ all): " + str(middle_pixels_count_not_0) + "/" + str(middle_pixels_count))
    print("right pixels count (not 0/ all): " + str(right_pixels_count_not_0) + "/" + str(right_pixels_count))
    left_divided = float(left_pixels_count_not_0)/float(left_pixels_count)
    middle_divided = float(middle_pixels_count_not_0)/float(middle_pixels_count)
    right_divided = float(right_pixels_count_not_0)/float(right_pixels_count)

    if left_divided > middle_divided and left_divided > right_divided:
        return "left"
    elif middle_divided > left_divided and middle_divided > right_divided:
        return "middle"
    elif right_divided > left_divided and right_divided > middle_divided:
        return "right"
    else:
        return "unknown"


def show_kinect_frame(disp_size, pixels, roiCoordinates, width, height):
    screen = pygame.display.set_mode(disp_size)
    temp_surface = pygame.Surface(disp_size)
    pygame.surfarray.blit_array(temp_surface, pixels)
    pygame.transform.scale(temp_surface, disp_size, screen)
    # Rect(left, top, width, height)
    rect = pygame.Rect(roiCoordinates.left, roiCoordinates.top,
     width, height)
    pygame.draw.rect(screen,(0,0,255),rect, 4)
    pygame.display.flip()


def my_main():
    # gamma = make_gamma()
    gamma = make_gamma_only_closest()
    disp_size = (640, 480)
    pygame.init()
    screen = pygame.display.set_mode(disp_size)
    roiCoordinates = RegionOfInterest(20, 600, 300, 350)

    width = roiCoordinates.right-roiCoordinates.left
    height = roiCoordinates.bottom-roiCoordinates.top
    while True:
        events = pygame.event.get()
        for e in events:
            if e.type == pygame.QUIT:
                sys.exit()
        # draw the pixels

        depth_frame = get_depth()
        # get only the closest pixels
        pixels = get_colorful_depth(depth_frame, gamma)

        most_occupied_part = which_rect_part_is_occupied_the_most(pixels, width, height, roiCoordinates)
        print(most_occupied_part)

        #sys.exit(0)
        show_kinect_frame(disp_size, pixels, roiCoordinates, width, height)


if __name__ == "__main__":
    my_main()
