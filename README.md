# edu-vr

## Specification
This project uses output from camera and recognizes three hand gestures:
   * left
   * right
   * straight
   * (and nothing)

You can either use your laptop camera or XBOX Kinect version 1.

## Usage
Setup the environment:
```
$ ./tasks dojo
$ ./tasks deps
# run this just once
$ sudo freenect-glview
```

Then, run this project using Kinect:
```
$ ./tasks run_tetris
```

or with your laptop camera (instead of Kinect):
```
$ ./tasks run_tetris_laptop
```

On the terminal you should see:
```
Kinect enabled: False
[STATUS] please wait! calibrating...
[STATUS] calibration successful...
```

You should see a new window which shows the output from your camera/Kinect and
 a green rectangle on it. In order to get best accuracy of hand gestures recognition,
 set your camera/Kinect in such a way that the green rectangle points entirely to
 some flat surface, e.g. a wall.

For a few seconds you should not do anything, then, after the `calibration successful`
 information appers, you may present some hand gestures. Put your hand in the green
 rectangle. The red caption should print the name of the recognized gesture.

Examples:
![](images/left.png)
![](images/right.png)
![](images/straight.png)
![](images/nothing.png)

The little colorful circles represent the extreme top, right, bottom and left points of the hand.

The captions next to the recognized hand gesture name represent the differences in relation to the bottom extreme point (see Theory below).

To stop the program press Ctrl+C in terminal.


## Troubleshooting
### Install Kinect support
If you use laptop's camera, you can omit this.

If you use Kinect, follow the steps from: https://naman5.wordpress.com/2014/06/24/experimenting-with-kinect-using-opencv-python-and-open-kinect-libfreenect/

### Check that Kinect is visible
[source](https://gist.github.com/llamapope/890f15d0c81a1ce6075b016cd938ca4b):
```
$ lsusb | grep Xbox
Bus 001 Device 009: ID 045e:02ae Microsoft Corp. Xbox NUI Camera
Bus 001 Device 007: ID 045e:02b0 Microsoft Corp. Xbox NUI Motor
Bus 001 Device 008: ID 045e:02ad Microsoft Corp. Xbox NUI Audio
```

Run such smoke tests to check that Kinect works:
```
$ sudo freenect-glview
$ sudo python kinect_test.py
```

### Docker
If running in docker container, run also this first:
```
$ xhost +"local:docker@"
```

[source](https://github.com/jessfraz/dockerfiles/issues/329)


## Links
This project is heavily inspired on:
* https://naman5.wordpress.com/2014/06/24/experimenting-with-kinect-using-opencv-python-and-open-kinect-libfreenect/ - installation
* blog post https://gogul.dev/software/hand-gesture-recognition-p1 and code: https://github.com/Gogul09/gesture-recognition/blob/master/segment.py
* https://openkinect.org/wiki/Python_Wrapper
* https://github.com/amiller/libfreenect-goodies - obsolete
* https://www.youtube.com/watch?v=kNfKIVMBxEE and https://github.com/SaranshKejriwal/kinect_nav
* https://www.youtube.com/watch?v=v-XcmsYlzjA and https://github.com/Sadaival/Hand-Gestures - remove `"_,"` in line 42, gestures.py, no kinect needed
* https://jonnoftw.github.io/2017/01/27/libfreenect-python-depth-image - kinect, depth, fps, ttf
* https://github.com/akshaybahadur21/Emojinator/tree/master/Rock_Paper_Scissor_Lizard_Spock - python rock paper scissors, CNN
* https://vipulsharma20.blogspot.com/2015/03/gesture-recognition-using-opencv-python.html

## Theory
1. Foreground and background separation steps:
   1. running averages, Background Subtraction (first see the background, then background + hand, the subtraction result is the hand)
   2. Motion Detection and Thresholding
      * Motion Detection = To detect the hand region from this difference image, we need to threshold the difference image, so that only our hand region becomes visible and all the other unwanted regions are painted as black.
      * Thresholding is the assignment of pixel intensities to 0's and 1's based a particular threshold level so that our object of interest alone is captured from an image.
   3. Contour Extraction (done by opencv)
2. Hand gestures recognition is done by:
   1. computing the extreme top, right, bottom, left points of the hand
   2. computing then the differences between right and bottom points and then left and bottom points. If those 2 differences are similar, then assume that we've got the "straight" gesture. Otherwise, depending on which is bigger, we get either "right" or "left" hand gesture.
