import cv2
import imutils
import getopt
import sys

class RegionOfInterest:
    def __init__(self, top, right, bottom, left):
        self.top = top
        self.right = right
        self.bottom = bottom
        self.left = left

def get_one_frame(kinect_enabled):
    if kinect_enabled:
        import kinect as kinect
        # get a current frame from RGB camera from Kinect
        frame = kinect.get_video()
    else:
        # get a current frame from camera (e.g. laptop camera)
        (_, frame) = camera.read()
    return frame


def get_kinect_depth(kinect_enabled):
    depth = None
    if kinect_enabled:
        import kinect as kinect
        depth = kinect.get_depth()
    return depth


#--------------------------------------------------
# To find the running average over the background
#--------------------------------------------------
def run_avg(image, accumWeight):
    global bg
    # initialize the background
    if bg is None:
        bg = image.copy().astype("float")
        return

    # compute weighted average, accumulate it and update the background
    cv2.accumulateWeighted(image, bg, accumWeight)

#---------------------------------------------
# To segment the region of hand in the image
# Returns:
# * thresholded
# * segmented - contours
#---------------------------------------------
def segment(image, threshold=25):
    global bg
    # find the absolute difference between background and current frame
    diff = cv2.absdiff(bg.astype("uint8"), image)

    # threshold the diff image so that we get the foreground;
    # In very basic terms, thresholding is like a Low Pass Filter by allowing only particular color ranges to be highlighted as white while the other colors are suppressed by showing them as black.
    thresholded = cv2.threshold(diff, threshold, 255, cv2.THRESH_BINARY)[1]

    # get the contours in the thresholded image;
    # we use the thresholded image for better accuracy
    #  Contours is a Python list of all the contours in the image. Each individual contour is a Numpy array of (x,y) coordinates of boundary points of the object.
    (cnts, _) = cv2.findContours(thresholded.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # return None, if no contours detected
    if len(cnts) == 0:
        return
    else:
        # based on contour area, get the maximum contour which is the hand
        segmented = max(cnts, key=cv2.contourArea)
        return (thresholded, segmented)

#--------------------------------------------------------------
# To recognize the hand gesture in the segmented hand region
#--------------------------------------------------------------
def recognize_hand_gesture(segmented, image, roi):
    # find the convex hull of the segmented hand region;
    # The convex points are generally, the tip of the fingers. But there are other convex point too.
    # Returns a collection of points. Each point has 2D coordinates. (So it's a 3D coll)
    # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_contours/py_contour_features/py_contour_features.html
    # https://www.pyimagesearch.com/2016/04/11/finding-extreme-points-in-contours-with-opencv/
    chull = cv2.convexHull(segmented, returnPoints=True)

    # find the most extreme points in the convex hull
    extreme_top    = tuple(chull[chull[:, :, 1].argmin()][0])
    extreme_bottom = tuple(chull[chull[:, :, 1].argmax()][0])
    # finds the smallest x-coordinate (i.e., the west value) in the entire contour array
    extreme_left   = tuple(chull[chull[:, :, 0].argmin()][0])
    extreme_right  = tuple(chull[chull[:, :, 0].argmax()][0])

    # many_extreme_bottom_points = tuple(chull[chull[:, :, 0]])
    # x_sum = 0
    # for point in many_extreme_bottom_points:
    #     # get only x coordinate
    #     x_sum += point[0]
    # avg_bottom_point = float(x_sum)/float(100.0)

    # draw a circle in the place of exterme top point;
    # The parameters here are the image/frame, the center of the circle, the radius, color BGR, and then thickness
    # (0,255,0) green
    # we could draws it like this:
    # cv2.circle(image,extreme_top, 5, (0,0,255), -1)
    # but we need to move it to he green rectangle
    cv2.circle(image,(extreme_top[0]+roi.right, extreme_top[1]+roi.top), 5, (0,255,0), -1)
    # (0,0,255) red
    cv2.circle(image,(extreme_bottom[0]+roi.right, extreme_bottom[1]+roi.top), 5, (0,0,255), -1)
    # (255,255,0) yellow
    cv2.circle(image,(extreme_left[0]+roi.right, extreme_left[1]+roi.top), 5, (255,255,0), -1)
    # (0,0,0) white
    cv2.circle(image,(extreme_right[0]+roi.right, extreme_right[1]+roi.top), 5, (0,0,0), -1)
    # # (0,0,0) white
    # cv2.circle(image,(avg_bottom_point+roi.right, extreme_bottom[1]+roi.top), 5, (255,255,255), -1)

    # # find the center of the palm
    # cX = int((extreme_left[0] + extreme_right[0]) / 2)
    # cY = int((extreme_top[1] + extreme_bottom[1]) / 2)

    # Let's comput the distance of the most left and most right points in
    # relation to the bottom point. Do not compute in relation to the center point,
    # because it always changes and the distances from the center and to the
    # left and to the right are both the same.
    distance_left_bottom = abs(extreme_left[0] - extreme_bottom[0])
    distance_right_bottom = abs(extreme_right[0] - extreme_bottom[0])
    if abs(distance_right_bottom - distance_left_bottom) < 60:
        return "straight " + str(distance_left_bottom) + " " + str(distance_right_bottom)
    elif distance_right_bottom < distance_left_bottom:
        return "left " + str(distance_left_bottom) + " " + str(distance_right_bottom)
    else:
        return "right " + str(distance_left_bottom) + " " + str(distance_right_bottom)

def get_events_as_hand_gesture(kinect_enabled, num_frames, roiCoordinates, accumWeight):
    frame = get_one_frame(kinect_enabled)

    # resize the frame
    frame = imutils.resize(frame, width=700)

    # flip the frame so that it is not the mirror view
    frame = cv2.flip(frame, 1)

    # clone the frame
    clone = frame.copy()

    # get the height and width of the frame
    (height, width) = frame.shape[:2]

    # get the ROI
    roi = frame[roiCoordinates.top:roiCoordinates.bottom,
          roiCoordinates.right:roiCoordinates.left]

    # convert the roi to grayscale and blur it. Why?
    # We convert an image from RGB to grayscale and then to binary in order to find the ROI i.e. the portion of the image we are further interested for image processing. By doing this our decision becomes binary: "yes the pixel is of interest" or "no the pixel is not of interest".
    gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
    # We blur the image for smoothing and to reduce noise and details from the image
    gray = cv2.GaussianBlur(gray, (7, 7), 0)

    gesture_name = "nothing"

    depth = get_kinect_depth(kinect_enabled)
    depth_clone = None
    if kinect_enabled:
        # flip the frame so that it is not the mirror view
        depth = cv2.flip(depth, 1)
        depth_clone = depth.copy()

    # to get the background, keep looking till a threshold is reached
    # so that our weighted average model gets calibrated
    if num_frames < 30:
        run_avg(gray, accumWeight)
        if num_frames == 1:
            print("[STATUS] please wait! calibrating...")
        elif num_frames == 29:
            print("[STATUS] calibration successful...")
    else:
        # segment the hand region
        hand = segment(gray)

        # check whether hand region is segmented
        if hand is not None:
            # if yes, unpack the thresholded image and
            # segmented region
            (thresholded, segmented) = hand

            # draw the segmented region and display the frame
            # https://docs.opencv.org/master/d4/d73/tutorial_py_contours_begin.html
            cv2.drawContours(clone, [segmented + (roiCoordinates.right, roiCoordinates.top)], -1, (0, 0, 255))

            gesture_name = recognize_hand_gesture(segmented, clone, roiCoordinates)

            # show the thresholded image (put it on the fixed location
            # on the screen at 40x30, so that it does not hide other windows)
            winname = "Thesholded"
            cv2.namedWindow(winname)  # Create a named window
            cv2.moveWindow(winname, 1550, 450)  # Move it
            cv2.imshow(winname, thresholded)

    return gesture_name, clone, depth_clone


def configure_kinect_options(argv):
    global bg, camera
    # global variables
    bg = None
    # get the reference to the webcam
    camera = cv2.VideoCapture(0)
    kinect_enabled = True
    help_msg = __file__ + ' -d true'

    try:
        opts, args = getopt.getopt(argv,"hd:",["disable-kinect="])
    except getopt.GetoptError:
        print(help_msg)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(help_msg)
            sys.exit()
        elif opt in ("-d", "--disable-kinect"):
            kinect_enabled = False
    print('Kinect enabled: %s' % kinect_enabled)

    # initialize accumulated weight
    accumWeight = 0.5
    # region of interest (ROI) coordinates
    roiCoordinates = RegionOfInterest(10, 350, 225, 590)
    # initialize num of frames
    num_frames = 0

    return kinect_enabled, accumWeight, roiCoordinates, num_frames


def main(argv):
    kinect_enabled, accumWeight, roiCoordinates, num_frames = configure_kinect_options(argv)
    # keep looping, until interrupted
    while(True):
        gesture_name, clone, depth_clone = get_events_as_hand_gesture(kinect_enabled, num_frames, roiCoordinates, accumWeight)

        cv2.putText(clone, str(gesture_name), (70, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)
        # draw the segmented hand
        cv2.rectangle(clone,
            (roiCoordinates.left, roiCoordinates.top),
            (roiCoordinates.right, roiCoordinates.bottom), (0,255,0), 2)

        # increment the number of frames
        num_frames += 1

        # display the frame with segmented hand
        cv2.imshow("Video Feed", clone)

        if kinect_enabled:
            cv2.imshow("Depth Feed", depth_clone)

        # observe the keypress by the user
        keypress = cv2.waitKey(1) & 0xFF

        # if the user pressed "q", then stop looping
        if keypress == ord("q"):
            break

    # free up memory
    camera.release()
    cv2.destroyAllWindows()


#-----------------
# MAIN FUNCTION
#-----------------
if __name__ == "__main__":
    main(sys.argv[1:])
